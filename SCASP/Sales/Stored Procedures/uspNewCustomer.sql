﻿CREATE PROCEDURE [Sales].[uspNewCustomer]  
@CustomerName NVARCHAR (40)  
AS 

SET NOCOUNT ON

 INSERT INTO [Sales].[Customer] (CustomerName) VALUES (@CustomerName); 


SELECT SCOPE_IDENTITY()  
