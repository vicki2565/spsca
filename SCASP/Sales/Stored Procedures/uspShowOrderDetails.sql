﻿CREATE PROCEDURE [Sales].[uspShowOrderDetails]  
@CustomerID INT=0  
AS  
BEGIN  

SET NOCOUNT ON

SELECT CustomerName,C.CustomerID,C.YTDOrders, C.YTDSales
  FROM [Sales].[Customer] AS C  
  INNER JOIN [Sales].[Orders] AS O  
     ON [O].[CustomerID] = [C].[CustomerID]  
  WHERE [C].[CustomerID] = @CustomerID 
  
  --WAITFOR DELAY '00:00:00.010'
END