﻿USE SCASP5

EXEC sp_configure 'clr enabled', 1;
RECONFIGURE;
DECLARE @cmd NVARCHAR(MAX);
SET @cmd='ALTER DATABASE ' + QUOTENAME(DB_NAME()) + ' SET TRUSTWORTHY ON;';
EXEC(@cmd);



--EXEC tSQLt.RunAll
EXEC tSQLt.RunTestClass N'MBTEST2';
GO

DECLARE @CHR_CONTENT  NVARCHAR(MAX),
        @CHR_TESTCASE NVARCHAR(MAX),
		@CHR_NAME     NVARCHAR(MAX),
		@CHR_RESULT   NVARCHAR(MAX),
		@CHR_MSG      NVARCHAR(MAX)

SET @CHR_CONTENT = '<Table><tr><td>TestCase</td><td>SP Name</td><td>Result</td><td>Msg</td></tr>'

DECLARE TESTRESULT Cursor FOR
SELECT TestCase, Name, Result, Msg 
  FROM tSQLt.TestResult WITH(NOLOCK)


Open TESTRESULT
Fetch NEXT FROM TESTRESULT INTO @CHR_TESTCASE, @CHR_NAME,@CHR_RESULT, @CHR_MSG
While (@@FETCH_STATUS <> -1)
BEGIN
  SET @CHR_CONTENT = @CHR_CONTENT + 
                     '<tr><td>' + @CHR_TESTCASE + '</td><td>' + @CHR_NAME + '</td><td>' + @CHR_RESULT + '</td><td>' + @CHR_MSG + '</td></tr>'

Fetch NEXT FROM TESTRESULT INTO @CHR_TESTCASE, @CHR_NAME,@CHR_RESULT, @CHR_MSG
END

CLOSE TESTRESULT
DEALLOCATE TESTRESULT

SET @CHR_CONTENT = @CHR_CONTENT + '</table>'

EXEC msdb.dbo.sp_send_dbmail
 @profile_name= 'SPTEST',
 @recipients='h925103@gmail.com',
 @subject=N'TEST',
 @body= @CHR_CONTENT,
 --@body= N'<Table><tr><td>TestCase</td><td>SP Name1</td><td>Result</td><td>Msg</td></tr>
 --                <tr><td>test that ConvertCurrency converts using given conversion rate</td><td>[LabHello].[test that ConvertCurrency converts using given conversion rate]</td><td>Failure</td><td>Expected: <2.40> but was: <3.20></td></tr></Table>',
 @body_format= 'HTML';
