﻿CREATE PROCEDURE [dbo].[USP_MBLOGIN_S00]
	@I_CHR_USERID        VARCHAR(20),      --使用者ID
	--@I_CHR_PWD           VARCHAR(20)       --使用者密碼
	@I_CHR_PASSWORD      VARCHAR(20),      --使用者密碼
	@O_CHR_MESSAGE       NVARCHAR(50) OUTPUT,
    @O_INT_MSGCODE       INT          OUTPUT
AS
  DECLARE @INT_ROWCOUNT  INT

SET NOCOUNT ON

BEGIN TRY
	SELECT USERID, USERNM, TEL, MOBILE, ADDRS
	  FROM MBUSERMF
	 WHERE USERID = @I_CHR_USERID
	   AND PWD = @I_CHR_PASSWORD

END TRY
BEGIN CATCH
  SET @O_INT_MSGCODE = 1
  SET @O_CHR_MESSAGE = ERROR_MESSAGE()

END CATCH
