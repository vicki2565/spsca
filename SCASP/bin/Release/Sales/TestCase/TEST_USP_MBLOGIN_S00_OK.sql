﻿CREATE PROCEDURE [MBTEST2].[TEST_USP_MBLOGIN_S00_OK]
AS 
CREATE TABLE #RESULT_USER
(
   USERID    VARCHAR(20),  
   USERNM    NVARCHAR(20),
   MOBILE    VARCHAR(20),
   TEL       VARCHAR(20),
   ADDRS     NVARCHAR(50)
)
BEGIN
  /* 實際資料 */
  DECLARE @CHR_USERID_ACT       VARCHAR(20)    --使用者ID_實際查詢

  /* 預設資料 */
  DECLARE @CHR_USERID            VARCHAR(20),  --使用者ID_測試用
          @CHR_PASSWORD          VARCHAR(20),  --密碼_測試用
          @O_CHR_MESSAGE         NVARCHAR(50),
          @O_INT_MSGCODE         INT

SET @CHR_USERID = 'A00001'
SET @CHR_PASSWORD = '1234'
SET @O_CHR_MESSAGE = ''
SET @O_INT_MSGCODE = 0

/* 1.寫入測試資料 */
INSERT INTO dbo.MBUSERMF(USERID, PWD, USERNM, MOBILE, TEL, ADDRS)
                  VALUES(@CHR_USERID, @CHR_PASSWORD, N'測試帳號', '', '', '')

/* 2.執行SP */
INSERT INTO #RESULT_USER(USERID,  
                         USERNM,
                         MOBILE,
                         TEL,
                         ADDRS)
EXEC dbo.USP_MBLOGIN_S00 @CHR_USERID,        --使用者ID
       	                 @CHR_PASSWORD,      --使用者密碼
	                     @O_CHR_MESSAGE  OUTPUT,
                         @O_INT_MSGCODE  OUTPUT


/* 3.取得執行SP結果 */
SET @CHR_USERID_ACT = ISNULL((SELECT USERID
                                FROM #RESULT_USER), '')


/* 4.執行tSQLt */
EXEC tSQLt.AssertEquals @CHR_USERID, @CHR_USERID_ACT


/* 5.刪除暫除資料表 */
DROP TABLE #RESULT_USER

END

