﻿USE SCASP5

EXEC sp_configure 'clr enabled', 1;
RECONFIGURE;
DECLARE @cmd NVARCHAR(MAX);
SET @cmd='ALTER DATABASE ' + QUOTENAME(DB_NAME()) + ' SET TRUSTWORTHY ON;';
EXEC(@cmd);

EXEC tSQLt.RunAll

SELECT TestCase, Name, Result, Msg FROM tSQLt.TestResult WITH(NOLOCK)


EXEC msdb.dbo.sp_send_dbmail
 @profile_name= 'SPTEST',
 @recipients='h925103@gmail.com',
 @subject=N'TEST',
 @body= N'<Table><tr><td>TestCase</td><td>SP Name1</td><td>Result</td><td>Msg</td></tr>
                 <tr><td>test that ConvertCurrency converts using given conversion rate</td><td>[LabHello].[test that ConvertCurrency converts using given conversion rate]</td><td>Failure</td><td>Expected: <2.40> but was: <3.20></td></tr></Table>',
 @body_format= 'HTML';
